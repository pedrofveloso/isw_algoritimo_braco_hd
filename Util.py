# -*- coding: utf-8 -*-

class Util:

    def preparar(self, fila, posicaoAtualLeitor):
        fila = self.converterParaInt(fila)
        fila.append(posicaoAtualLeitor)
        fila.sort()
        return fila

    def converterParaInt(self, fila):
        ret = []
        for i in fila:
            ret.append(int(i))

        return ret