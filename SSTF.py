# -*- coding: utf-8 -*-
import Util

class SSTF:
    primeiraPosicao = 0
    fila = []

    def __init__(self, posicaoInicialLeitor, fila):
        self.posicaoAtualLeitor = posicaoInicialLeitor
        self.fila = Util.Util().preparar(fila, self.posicaoAtualLeitor)
        proximaPosicao = self.pegarProximaPosicao()
        self.executar(proximaPosicao)

    def pegarProximaPosicao(self):
        #contencao
        if self.posicaoAtualLeitor in self.fila:
            indice = self.fila.index(self.posicaoAtualLeitor)
            proximaPosicao = 0
            
            if(len(self.fila) == 1):
                proximaPosicao = -1
            elif(indice == 0):
                proximaPosicao = self.fila[indice+1]
            elif(indice == len(self.fila)-1):
                proximaPosicao = self.fila[indice-1]
            else:
                anterior = self.fila[indice-1]
                proximo = self.fila[indice+1]

                diferencaAnterior = self.posicaoAtualLeitor - anterior
                diferencaProximo = proximo - self.posicaoAtualLeitor

                if(diferencaAnterior <= diferencaProximo):
                    proximaPosicao = anterior
                else:
                    proximaPosicao = proximo

            #print("pegarProxPos - retorno:", proximaPosicao)
            
            return proximaPosicao
    
    def executar(self, proximaPosicao):
        if(proximaPosicao == -1): return
        
        #print("executar - proxPos: ", proximaPosicao)
        if(proximaPosicao < self.posicaoAtualLeitor):
            #print("seekEsquerda: ", proximaPosicao)
            self.seekEsquerda(proximaPosicao)
        else:
            #print("seekDireita: ", proximaPosicao)
            self.seekDireita(proximaPosicao)

        self.fila.remove(self.posicaoAtualLeitor)
        self.posicaoAtualLeitor = proximaPosicao
        if(len(self.fila) > 0):
            proximaPosicao = self.pegarProximaPosicao()
            self.executar(proximaPosicao)
        

    def seekEsquerda(self, proximaPosicao):
        for i in reversed(range(proximaPosicao, self.posicaoAtualLeitor)):
            if(i != proximaPosicao):
                print("seeked -> ", i)
            else:
                print("READ -> ", i)



    def seekDireita(self, proximaPosicao):        
        for i in range(self.posicaoAtualLeitor+1, proximaPosicao+1):
            if(i != proximaPosicao):
                print("seeked -> ", i)
            else:
                print("READ -> ", i)

        
    
                
