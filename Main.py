# -*- coding: utf-8 -*-

from enum import Enum
import SSTF
import Elevador


class AlgType(Enum):
    SSTF = 1
    ELEVADOR = 2

def main():
    print("ALGORITIMO ESCALONAMENTO")
    print("Selecione o tipo de Escalonamento: ")
    print("1 - SSTF \n2- Elevador")
    op = raw_input("opção: ")

    if(op == "1"):
        criarFila(AlgType.SSTF, None)
    elif(op == "2"):
        selecionarVariante(AlgType.ELEVADOR)
    else:
        print(">>Opção inválida!<<")
        main()


def selecionarVariante(algoritimo):
    print("---")
    print("Selecione a variante:")
    print("1 - SCAN \n2-LOOK")
    variante = raw_input("opção:")
    if(variante != "1" and variante != "2"):
        print(">>Opção inválida!<<")
        selecionarVariante(algoritimo)
    else:
        criarFila(algoritimo, variante)


def criarFila(algoritimo, variante):
    print("--- : ", algoritimo)
    fila = raw_input("Digite as posições de leitura (separados por virgula): ")
    fila.replace(" ", "")
    arrayPosicoes = fila.split(",")
    executarAlgoritimo(algoritimo, arrayPosicoes, variante)

def executarAlgoritimo(algoritimo, fila, variante):
    posicaoInicial = selecionarPosicaoInicialCabecote()
    if(algoritimo == AlgType.SSTF):
        SSTF.SSTF(posicaoInicial, fila)
    else:
        elevador = Elevador.Elevador(posicaoInicial)
        elevador.preparar(fila, variante)

def selecionarPosicaoInicialCabecote():
    posicaoInicial = raw_input("Selecioine a posição inicial do cabeçote (de 0 à 39): ")
    if( 0 <= int(posicaoInicial) <= 39 ):
        return int(posicaoInicial)
    else:
        print(">>Opção inválida!<<")
        return selecionarPosicaoInicialCabecote()

if __name__ == "__main__":
    main()
